﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWonBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject objectToActivate;

    private void Awake()
    {
        ZombieTracker.GameWon += ZombieTracker_GameWon;
    }

    private void OnDestroy()
    {
        ZombieTracker.GameWon -= ZombieTracker_GameWon;
    }

    void ZombieTracker_GameWon()
    {
        objectToActivate.SetActive(true);
    }

}
