﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float lifeDuration = 3f;
    float time;

    bool alreadyUsed;

    private void OnCollisionEnter(Collision collision)
    {
        ICurable curable = collision.gameObject.GetComponent<ICurable>();

        if (curable != null && !alreadyUsed)
        {
            curable.Cure();
            alreadyUsed = true;
        }
    }

    private void Update()
    {
        time += Time.deltaTime;

        if (time > lifeDuration)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
