﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine; //Remember the using statement before the class declaration
using UnityEngine.AI;
using System.Linq;

public class FriendlyBehavior : MonoBehaviour
{
    [AutoFind]
    public NavMeshAgent agent;

    [AutoFind]
    public Rigidbody rb;

    [AutoFind]
    public Animator animator;

    public float maxDistanceForBeingScared = 10f;
    public float maxDistanceForReachingGoal = 1f;
    public float stunnedDuration = 2f;
    private float howLongHaveIBeenRoamingIcanScareclyRememmber = 0f;
    public float roamingTimeout = 8f;
    const float tooFar = 9999;
    public float walkRadius = 10f;
    private Vector3 roamingGoal;

    public float runningSpeed = 5f;
    public float roamingSpeed = 1f;
    public float thresholdVelocityForEndingStun = 0.2f; 

    private StateMachine<States> fsm;

    public enum States
    {
        Roaming,
        RunningAway,
        BeingEaten,
        Stunned,
        Disabled
    }

    void Awake()
    {
        fsm = StateMachine<States>.Initialize(this);
        fsm.ChangeState(States.Roaming, StateTransition.Overwrite);
    }

    void OnDisable()
    {
        //Debug.Log("PrintOnDisable: friendly script was disabled");
        if (fsm != null)
        {
            fsm.ChangeState(States.Disabled, StateTransition.Overwrite);
        }
    }

    void OnEnable()
    {
        //Debug.Log("PrintOnEnable: friendly script was enabled");
        if (fsm != null)
        {
            animator.SetTrigger("Yay");
            fsm.ChangeState(States.Roaming, StateTransition.Overwrite);
        }
    }

    public void Cure()
    {
        animator.SetTrigger("Yay");
    }

    private void SetRandomGoal()
    {
        Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
        randomDirection += agent.transform.position;
        NavMesh.SamplePosition(randomDirection, out NavMeshHit hit, walkRadius, 1);
        roamingGoal = hit.position;
        howLongHaveIBeenRoamingIcanScareclyRememmber = 0;
        agent.destination = roamingGoal;
        //Debug.Log("New random goal " + roamingGoal);
    }

    void Roaming_Enter()
    {
        // Start roaming. Pick a goal, any goal
        //Debug.Log("Roaming");
        animator.SetBool("Moving", true);
        animator.SetBool("Fleeing", false);
        SetRandomGoal();

        agent.speed = roamingSpeed;
    }

    void Roaming_Update()
    {
        howLongHaveIBeenRoamingIcanScareclyRememmber += Time.deltaTime;
        if (IsZombieTooClose())
            fsm.ChangeState(States.RunningAway, StateTransition.Overwrite);
        else if (IsGoalReached() || howLongHaveIBeenRoamingIcanScareclyRememmber > roamingTimeout)
            // We stopped roaming, pick a new goal
            // (we are basicalyl reentering roaming here)
            SetRandomGoal();
    }

    void RunningAway_Enter()
    {
        //idk
        //Debug.Log("RUN");
        SetKinematic(true);
        animator.SetBool("Moving", true);
        animator.SetBool("Fleeing", true);

        agent.speed = runningSpeed;
    }

    void RunningAway_Update()
    {
        //run away from nearest zombie
        // YAY
        // Fleeing
        //Moving
        animator.SetBool("Moving", true);
        animator.SetBool("Fleeing", true);
        if (IsZombieTooClose())
        {
            agent.destination = RunFromZombieGoal();
            //Debug.Log("RUN TO " + agent.destination);
        }
        else
        {
            //Oh, I guess we're safe now
            fsm.ChangeState(States.Roaming, StateTransition.Overwrite);
        }

    }

    void BeingEaten_Enter()
    {
        //start transition
        animator.SetBool("Moving", false);
        Debug.Log("Oh no, I'm lost");
    }

    Person FindNearestZombie()
    {
        var zombies = ZombieTracker.GetZombies();

        if (zombies != null && zombies.Any())
            return zombies.OrderBy(DistanceFrom)
                     .First();

        else
        {
            return null;
        }
    }

    bool IsZombieTooClose()
    {
        var zombie = FindNearestZombie();
        if (zombie == null)
            return false;
        return DistanceFrom(zombie) < maxDistanceForBeingScared;
    }

    Vector3 RunFromZombieGoal()
    {
        var zombie = FindNearestZombie();
        Vector3 escapeVector = (transform.position - zombie.transform.position);
        escapeVector.Normalize();
        escapeVector *= maxDistanceForBeingScared;
        return zombie.transform.position + escapeVector;
    }


    bool IsGoalReached()
    {
        return (agent.transform.position - roamingGoal).magnitude < maxDistanceForReachingGoal;
    }

    float DistanceFrom(Person p)
    {
        if (p == null)
            return tooFar;
        return (p.transform.position - transform.position).magnitude;
    }

    public void OnCollisionEnterFromTheOutside(Collision collision)
    {
        //Debug.Log("Collision");

        var person = collision.gameObject.GetComponent<Person>();

        if (person != null)
        {
            if (person.CurrentState == ZombieState.Zombie)
                fsm.ChangeState(States.BeingEaten, StateTransition.Overwrite);

            if (person is Player)
            {
                SimulateCollision(collision);
            }
        }

        else if (rb != null && fsm != null && collision.gameObject.tag != "Ground")
        {
            SimulateCollision(collision);
        }
    }

    void SimulateCollision(Collision collision)
    {
        SetKinematic(false);
        rb.AddForce(-collision.impulse);
        fsm.ChangeState(States.Stunned, StateTransition.Overwrite);
    }

    IEnumerator Stunned_Enter()
    {
        Debug.Log("Stunned!");
        animator.SetBool("Moving", false);
        yield return new WaitForSeconds(stunnedDuration);
        yield return new WaitUntil(() => rb.velocity.magnitude < thresholdVelocityForEndingStun);

        SetKinematic(true);
        fsm.ChangeState(States.Roaming, StateTransition.Overwrite);
    }

    private void SetKinematic(bool flag)
    {
        // The order we set the flags is probably important
        // Never want to be enable NavMeshAgent without being Kinematic first
        if (flag)
        {
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            agent.enabled = true;
        }
        else
        {
            agent.enabled = false;
            rb.isKinematic = false;
        }
    }

    void Disable_Exit()
    {
        Debug.Log("Friendly enabled, set Kinematic flag for autonav");
        SetKinematic(true);
    }

}
