﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stasher : MonoBehaviour
{
    public KeyCode key = KeyCode.E;
    public bool Active { get; set; } = false;
    public RifleStash rifleModelPrefab;

    public float distanceInFront = 4f;

    [AutoFind]
    public Player player;

    private void Update()
    {
        if (Active)
        {
            if (Input.GetKeyDown(key))
            {
                StashWeapon();
            }
        }
    }

    public void StashWeapon()
    {
        Instantiate(rifleModelPrefab, GetStashPosition(), Random.rotation);
        player.ToggleRifle(false);

        Active = false;

        Debug.Log("stashed!");
    }

    Vector3 GetStashPosition()
    {
        return transform.TransformPoint(Vector3.forward * distanceInFront);
    }

}
