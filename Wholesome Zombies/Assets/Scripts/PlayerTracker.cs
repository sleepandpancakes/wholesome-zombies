﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class PlayerTracker
{
    static List<Player> players = new List<Player>();
    static List<HealthySpawn> nextSpawns = new List<HealthySpawn>();

    public delegate void GameEvent();
    public static event GameEvent GameLost = delegate {};

    static PlayerTracker()
    {
        //Player.PlayerInfectionStarted += Player_PlayerInfectionStarted;
        //Player.PlayerInfectionComplete += Player_PlayerInfectionComplete;
    }

    public static void AddPlayer(Player p)
    {
        players.Add(p);
    }

    public static void AddSpawn(HealthySpawn p)
    {
        nextSpawns.Add(p);
    }

    public static bool AreAllPlayersZombies()
    {
        foreach (Person p in players)
        {
            if (p.CurrentState == ZombieState.Healthy)
                return false;
        }

        return true;
    }

    public static bool AreThereMoreHealthySpawns => nextSpawns.Any();

    public static void Reset()
    {
        players.Clear();
    }

    public static IEnumerable<Player> GetHealthyPlayers()
    {
        return players.Where(p => p.CurrentState == ZombieState.Healthy);
    }

    public static Player GetRandomHealthyPlayer()
    {
        return GetHealthyPlayers()?.ToList().RandomElement();
    }

    public static HealthySpawn GetRandomSpawn()
    {
        return nextSpawns.RandomElement();
    }

    public static void SwitchControlTo(HealthySpawn spawn, Player oldPlayer)
    {
        spawn.gameObject.SetActive(false);

        oldPlayer.ForceStash();
        oldPlayer.transform.position = spawn.transform.position;
        oldPlayer.Reset();

        nextSpawns.Remove(spawn);
    }

    //static void Player_PlayerInfectionStarted(Player p)
    //{
    //    Debug.Log("ur getting infected!");
    //}

    //static void Player_PlayerInfectionComplete(Player p)
    //{

    //}

    public static void ReportCompleteInfection(Player p)
    {
        Debug.Log("omg ur infected");

        if (AreThereMoreHealthySpawns)
        {
            SwitchControlTo(GetRandomSpawn(), p);
        }

        else
        {
            Debug.Log("you lose");
            GameLost();
        }
    }

}
