﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MonsterLove.StateMachine; //Remember the using statement before the class declaration
using UnityStandardAssets.Characters.FirstPerson;
using System.Linq;

public class EnemyBehavior : MonoBehaviour
{
    [AutoFind]
    public NavMeshAgent agent;

    [AutoFind]
    public Rigidbody rb;

    [AutoFind]
    public Animator animator;

    public float thinkingDuration = 2f;
    public float stunnedDuration = 2f;
    public float roamingSpeed = 1f;

    private StateMachine<States> fsm;
    private Vector3 shamblingGoal;


    public enum States
    {
        Shambling,
        Thinking,
        Stunned,
        Disabled //No longer enemy
    }

    void OnDisable()
    {
        //Debug.Log("PrintOnDisable: enemy script was disabled");
        if (fsm != null)
        {
            fsm.ChangeState(States.Disabled, StateTransition.Overwrite);
        }
    }

    void OnEnable()
    {
        //Debug.Log("PrintOnEnable: enemy script was enabled");
        if (fsm != null)
        {
            fsm.ChangeState(States.Thinking, StateTransition.Overwrite);
        }

        agent.speed = roamingSpeed;
    }

    void Awake()
    {
        fsm = StateMachine<States>.Initialize(this);
        fsm.ChangeState(States.Thinking, StateTransition.Overwrite);
    }

    IEnumerator Thinking_Enter()
    {
        //Debug.Log("We are now thinknig");
        animator.SetBool("Moving", false);

        yield return new WaitForSeconds(thinkingDuration);
        fsm.ChangeState(States.Shambling, StateTransition.Overwrite);

    }

    void Shambling_Enter()
    {
        Person target = FindNearestTarget();
        SetKinematic(true);
        animator.SetBool("Moving", true);

        if (target != null)
        {
            shamblingGoal = target.transform.position;
            agent.destination = shamblingGoal;
        }

        else
            agent.destination = transform.position;

        //Debug.Log("We are now shambling to " + shamblingGoal);
    }

    void Shambling_Update()
    {
        if (agent.velocity.magnitude < .1)
        {
            //Debug.Log("Goal crushed "  + shamblingGoal + "/" + agent.velocity.magnitude);
            fsm.ChangeState(States.Thinking, StateTransition.Overwrite);
        }

        //else
        //{
        //    //Debug.Log("Pending " + agent.velocity.magnitude);
        //}
    }

    Person FindNearestTarget()
    {
        var healthyPeople = ZombieTracker.GetHealthyPeople();

        if (healthyPeople != null && healthyPeople.Any())
            return healthyPeople.OrderBy(DistanceFrom)
                .First();

        else return null;
    }

    float DistanceFrom(Person p)
    {
        return (p.transform.position - transform.position).magnitude;
    }

    void Eat(Person person)
    {
        if (person.CurrentState == ZombieState.Zombie || person.Invulnerable)
            return;

        animator.SetTrigger("Attack");
        person.Infect();
        fsm.ChangeState(States.Thinking, StateTransition.Overwrite);
    }

    public void OnCollisionEnterFromTheOutside(Collision collision)
    {
        var person = collision.gameObject.GetComponent<Person>();

        //If collide with a person, eat
        if (person != null)
        {
            Eat(person);
        }

        else if (rb != null && fsm != null && collision.gameObject.tag != "Ground")
        {
            SetKinematic(false);
            rb.AddForce(-collision.impulse);
            fsm.ChangeState(States.Stunned, StateTransition.Overwrite);
        }
    }

    private void SetKinematic(bool flag)
    {
        // The order we set the flags is probably important
        // Never want to be enable NavMeshAgent without being Kinematic first
        if(flag)
        {
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            agent.enabled = true;
        }
        else
        {
            agent.enabled = false;
            rb.isKinematic = false;
        }
    }

    IEnumerator Stunned_Enter()
    {
        animator.SetBool("Moving", false);

        yield return new WaitForSeconds(stunnedDuration);

        SetKinematic(true);
        fsm.ChangeState(States.Thinking, StateTransition.Overwrite);
    }

    void Disable_Exit()
    {
        Debug.Log("Enemy enabled, set Kinematic flag for autonav");
        SetKinematic(true);
    }

}
