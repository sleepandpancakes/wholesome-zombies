﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public static class ZombieTracker
{
    static List<Person> people = new List<Person>();

    public delegate void GameEvent();
    public static event GameEvent GameWon = delegate { };

    static ZombieTracker()
    {
        SceneManager.sceneLoaded += SceneManager_SceneLoaded;
    }

    static void SceneManager_SceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        Reset();
    }

    public static void AddPerson(Person p)
    {
        people.Add(p);
    }

    public static bool AreAllPeopleHealthy()
    {
        foreach (Person p in people)
        {
            if (p.CurrentState == ZombieState.Zombie)
                return false;
        }

        return true;
    }

    public static bool AreAllPeopleZombies()
    {
        foreach (Person p in people)
        {
            if (p.CurrentState == ZombieState.Healthy)
                return false;
        }

        return true;
    }

    public static void Reset()
    {
        people.Clear();
    }

    public static IEnumerable<Person> GetZombies()
    {
        return people.Where(p => p.CurrentState == ZombieState.Zombie);
    }

    public static IEnumerable<Person> GetHealthyPeople()
    {
        return people.Where(p => p.CurrentState == ZombieState.Healthy);
    }

    public static void CheckIfGameWon()
    {
        if (AreAllPeopleHealthy())
            GameWon();
    }
}
