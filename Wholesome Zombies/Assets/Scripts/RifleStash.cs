﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleStash : MonoBehaviour
{
    //float height;
    Vector3 originalPosition;
    public float periodOfOscillation = 1f;
    public float height = 1f;
    public float amplitude = 1f;

    private void Awake()
    {
        //height = transform.position.y;
        originalPosition = transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var player = collision.gameObject.GetComponent<Player>();

        if (player != null && player.CurrentState == ZombieState.Healthy)
        {
            GivePlayerRifle(player);
        }
    }

    void GivePlayerRifle(Player p)
    {
        p.ToggleRifle(true);
        gameObject.SetActive(false);
        p.stasher.Active = true;

        Debug.Log("wow u have rifle now");
    }

    private void Update()
    {
        transform.position = new Vector3(
            originalPosition.x,
            Mathf.Sin(Time.time / periodOfOscillation) * amplitude + height,
            originalPosition.z
        );
    }
}
