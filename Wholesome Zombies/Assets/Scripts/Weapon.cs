﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [Space]
    public float launchSpeed = 10f;
    public int maxAmmoCount = 3;
    public float reloadDuration = 1f;

    float reloadTimer;
    public int CurrentAmmoCount { get; private set; }
    public event IntDelegate ChangeAmmoCountTo;
    public delegate void IntDelegate(int number);

    private void Awake()
    {
        CurrentAmmoCount = maxAmmoCount;

        ChangeAmmoCountTo += Weapon_ChangeAmmoCount;
    }

    private void Update()
    {
        if (IsPlayerInput() && CurrentAmmoCount > 0)
        {
            Shoot();
            ChangeAmmoCountTo(CurrentAmmoCount - 1);
        }

        if (CurrentAmmoCount < maxAmmoCount)
            Reload();
    }

    public abstract void Shoot();

    protected void Reload()
    {
        reloadTimer += Time.deltaTime;

        if (reloadTimer > reloadDuration)
        {
            if (CurrentAmmoCount < maxAmmoCount)
                ChangeAmmoCountTo(CurrentAmmoCount + 1);

            reloadTimer = 0;
        }
    }

    protected virtual void Weapon_ChangeAmmoCount(int number)
    {
        CurrentAmmoCount = number;
    }

    protected virtual bool IsPlayerInput()
    {
        return Input.GetMouseButtonDown(0);
    }
}
