﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncher : Weapon
{
    public Grenade grenadePrefab;
    public Transform spawnTransform;
    public Camera playerCamera;

    Grenade GetNewGrenade()
    {
        return Instantiate(grenadePrefab, spawnTransform.position, spawnTransform.rotation);
    }

    public override void Shoot()
    {
        var grenade = GetNewGrenade();

        grenade.rb.velocity = playerCamera.transform.forward * launchSpeed;
    }

    protected override bool IsPlayerInput()
    {
        return Input.GetButtonDown("Fire2");
    }
}
