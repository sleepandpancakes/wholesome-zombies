﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[SelectionBase]
public class Person : MonoBehaviour, IInfectable, ICurable
{
    public GameObject healthyVersion;
    public GameObject zombieVersion;
    public ZombieState CurrentState;

    public delegate void NPCEvent();
    public NPCEvent NPCInfected = delegate { };
    public NPCEvent NPCCured = delegate { };

    public FriendlyBehavior friendlyBrain;
    public EnemyBehavior enemyBrain;

    [ShowNativeProperty]
    public bool Invulnerable { get; private set; } = false;
    public float invulnerabilityDuration = 3f;

    [Button]
    public virtual void Cure()
    {
        if (CurrentState == ZombieState.Healthy)
            return;

        zombieVersion?.SetActive(false);
        healthyVersion?.SetActive(true);

        CurrentState = ZombieState.Healthy;

        StartCoroutine(_InvulnerabilityRoutine());
        NPCCured();

        ZombieTracker.CheckIfGameWon();
    }

    [Button]
    public virtual void Infect()
    {
        if (CurrentState == ZombieState.Zombie)
            return;

        if (zombieVersion != null && healthyVersion != null)
        {
            zombieVersion?.SetActive(true);
            healthyVersion?.SetActive(false);
        }

        CurrentState = ZombieState.Zombie;

        NPCInfected();
    }

    private void Start()
    {
        ZombieTracker.AddPerson(this);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (CurrentState == ZombieState.Healthy)
            friendlyBrain.OnCollisionEnterFromTheOutside(collision);

        else
            enemyBrain.OnCollisionEnterFromTheOutside(collision);
    }

    IEnumerator _InvulnerabilityRoutine()
    {
        Invulnerable = true;
        yield return new WaitForSeconds(invulnerabilityDuration);
        Invulnerable = false;
    }
}

public enum ZombieState { Healthy, Zombie }