﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    [AutoFind]
    public Rigidbody rb;
    public SphereCollider explosionSphere;

    [Space]
    public float detonateDuration = 2f;
    public float forceMagnitude = 50f;

    float time;

    public void Detonate()
    {
        Debug.Log("BOOM!");
        Destroy(gameObject);

        //Curing all zombies in sphere
        var objects = Physics.OverlapSphere(explosionSphere.transform.position, explosionSphere.radius);

        for (int i = 0; i < objects.Length; i++)
        {
            var curable = objects[i].GetComponent<ICurable>();
            if (curable != null)
            {
                curable.Cure();
                ApplyForceTo(objects[i].gameObject);
            }
        }
    }

    private void Update()
    {
        time += Time.deltaTime;

        if (time > detonateDuration)
            Detonate();
    }

    void ApplyForceTo(Rigidbody r)
    {
        var direction = (r.transform.position - transform.position + Vector3.up).normalized;
        r.AddForce(direction * forceMagnitude);
    }

    void ApplyForceTo(GameObject g)
    {
        var rigid = g.GetComponent<Rigidbody>();

        if (rigid != null)
            ApplyForceTo(rigid);

        else Debug.Log("no rigidbody");
    }

}
