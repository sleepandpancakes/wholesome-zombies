﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MEC;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;
using NaughtyAttributes;

public class Player : Person
{
    public float infectionDuration = 5f;
    public float InfectionTimeLeft { get; private set; }

    public delegate void PlayerEvent(Player p);
    public static event PlayerEvent PlayerInfectionStarted = delegate { };
    public static event PlayerEvent PlayerInfectionComplete = delegate { };

    public Rifle rifle;

    [AutoFind]
    public Stasher stasher;

    [AutoFind]
    public RigidbodyFirstPersonController playerController;

    [AutoFind]
    public Camera cam;

    private void Awake()
    {
        InfectionTimeLeft = infectionDuration;
    }

    [Button]
    public override void Infect()
    {
        Debug.Log($"you are infected quick u have {infectionDuration} seconds");
        CurrentState = ZombieState.Zombie;
        stasher.Active = true;
        //base.Infect();

        Timing.RunCoroutine(_InfectionRoutine());
    }

    IEnumerator<float> _InfectionRoutine()
    {
        PlayerInfectionStarted(this);

        //Waiting for time
        while (InfectionTimeLeft > 0)
        {
            InfectionTimeLeft -= Time.deltaTime;
            yield return 0f;
        }

        PlayerInfectionComplete(this);
        PlayerTracker.ReportCompleteInfection(this);
    }

    //public void ToggleNPCMode(bool toggle)
    //{
    //    agent.enabled = toggle;
    //    enemyBehavior.enabled = toggle;

    //    playerController.enabled = !toggle;
    //    cam.enabled = !toggle;
    //}

    //[Button]
    //public void MakeNPC()
    //{
    //    ToggleNPCMode(true);
    //}

    //[Button]
    //public void MakeTruePlayer()
    //{
    //    ToggleNPCMode(false);
    //}

    //public void SetAsActive()
    //{
    //    this.enabled = true;
    //    playerController.enabled = true;
    //    cam.enabled = true;
    //}
    public void Reset()
    {
        InfectionTimeLeft = infectionDuration;
        CurrentState = ZombieState.Healthy;
        //stasher.Active = true;
    }


    public void ToggleRifle(bool toggle)
    {
        rifle.gameObject.SetActive(toggle);
    }

    public override void Cure()
    {

    }

    protected override void OnCollisionEnter(Collision collision)
    {

    }

    public void ForceStash()
    {
        stasher.StashWeapon();
    }

}
