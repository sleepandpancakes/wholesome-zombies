﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Rifle : Weapon
{
    public Rigidbody bulletPrefab;
    public Transform bulletStartPosition;
    public Camera playerCamera;
    public LocalAudioManager localAudio;


    Rigidbody GetNewBullet()
    {
        return Instantiate(bulletPrefab, bulletStartPosition.position, bulletStartPosition.rotation);
    }

    public override void Shoot()
    {
        int n = UnityEngine.Random.Range(1 , 4);
        localAudio.Play("Pew" + n);

        var bullet = GetNewBullet();

        bullet.velocity = playerCamera.transform.forward * launchSpeed;
    }
}
