﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AmmoCountGrenade : MonoBehaviour
{
    public GrenadeLauncher grenade;

    public GameObject[] grenadeCountImage;

    private int grenadeCount;

    private float reloadTimer;
    public Image reloadBar;

    private void OnEnable()
    {
        grenadeCount = grenade.CurrentAmmoCount;
        grenade.ChangeAmmoCountTo += UpdateTheAmmo;
        //subscribe to rifle events
    }

    public void UpdateTheAmmo(int newAmmo)
    {

        Debug.Log("UPDATE AMMO Grenade; number of images: " +grenadeCountImage.Length);

        int n = grenadeCountImage.Length;
        while (n > 0 )
        {
            //Debug.Log("while loop; new ammo: "+newAmmo);

            if (n > newAmmo)
                grenadeCountImage[n-1].SetActive(false);
            else
                grenadeCountImage[n-1].SetActive(true);

            if (newAmmo == 0)
                grenadeCountImage[0].SetActive(false);

            n--;
        }


        grenadeCount = newAmmo;



    }



    public void SetTheAmmo(int ammo)
    {

        grenadeCount = grenade.CurrentAmmoCount;
        UpdateTheAmmo(grenadeCount);

    }

    public void ReloadBarUpdate()
    {
        reloadTimer += Time.deltaTime;
        reloadBar.fillAmount = reloadTimer / grenade.reloadDuration;

        if (reloadTimer > grenade.reloadDuration)
        {
            reloadTimer = 0;
        }
    }




    void Start()
    {
        SetTheAmmo(grenade.CurrentAmmoCount);
    }

    void Update()
    {

        if (grenade.CurrentAmmoCount < grenade.maxAmmoCount)
            ReloadBarUpdate();
        else
            reloadTimer = 0;
    }


}
