﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AmmoCountRifle : MonoBehaviour
{
    public Rifle rifle;
    public Text ammoText;
    private int ammoCount;

    public Image reloadBar;
    private float reloadTimer;


    private void OnEnable()
    {
        ammoCount = rifle.CurrentAmmoCount;
        rifle.ChangeAmmoCountTo += UpdateTheAmmo;
        //subscribe to rifle events
    }

    public void UpdateTheAmmo(int newAmmo)
    {

        //Debug.Log("UPDATE AMMO");
        ammoCount = newAmmo;
        ammoText.text = ammoCount + " X";
    }

   

    public void SetTheAmmo(int ammo)
    {

        ammoCount = ammo;
        ammoText.text =ammoCount + " X";

    }

    public void ReloadBarUpdate()
    {
        reloadTimer += Time.deltaTime;
        reloadBar.fillAmount = reloadTimer/rifle.reloadDuration;

        if (reloadTimer > rifle.reloadDuration)
        {
            reloadTimer = 0;
        }
    }




    void Start()
    {
        SetTheAmmo(rifle.CurrentAmmoCount);
    }
    
    void Update()
    {

        if (rifle.CurrentAmmoCount < rifle.maxAmmoCount)
            ReloadBarUpdate();
        else
            reloadTimer = 0;
    }


}
