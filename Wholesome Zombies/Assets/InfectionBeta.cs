﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class InfectionBeta : MonoBehaviour
{
    public AudioManager globalAudioManager;

    private void Awake()
    {
        globalAudioManager = FindObjectOfType<AudioManager>();
    }

    [Button]
    public void Infected()
    {
        globalAudioManager.FadeInPlay("Infection", 1);
    }
}
