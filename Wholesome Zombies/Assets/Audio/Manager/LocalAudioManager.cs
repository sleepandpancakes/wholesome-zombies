﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalAudioManager : MonoBehaviour
{
    public bool volumeOverrideBool;

    [Range (0f, 1f)]
    public float volumeOverride;

    public bool pitchOverrideBool;

    [Range(0f, 3f)]
    public float pitchOverride;

    public Sound[] sounds;



    // Start is called before the first frame update
    void Awake()
    {

        foreach (Sound s in sounds)
        {
            //rig together
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            // set parameters
            s.source.volume = s.volume;
            if (volumeOverrideBool)
                s.source.volume = volumeOverride;

            s.source.pitch = s.pitch;
            if (pitchOverrideBool)
                s.source.pitch = pitchOverride;

            s.source.loop = s.loop;
            s.source.spatialBlend = s.spacial;

            if (s.awakeBool)
                s.source.Play();

        }


    }


    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        s.source.Play();
    }

    //play at point
    public void Play(string name, Vector3 position, float volumeMultiplier)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        AudioSource.PlayClipAtPoint(s.clip, position, s.volume*volumeMultiplier);
    }
}