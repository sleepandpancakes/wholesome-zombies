﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class GameEventsAudio : MonoBehaviour
{

    [AutoFind]
    public AudioManager globalAudioManager;


    // Start is called before the first frame update
    void Start()
    {
        Player.PlayerInfectionStarted += Player_PlayerInfectionStarted;;
        Player.PlayerInfectionComplete += Player_PlayerInfectionComplete;
    }

    void Player_PlayerInfectionStarted(Player p)
    {


        if (PlayerTracker.AreThereMoreHealthySpawns)
            globalAudioManager.Play("Infection1");
        else
            globalAudioManager.Play("Infection2");

        globalAudioManager.Play("Ouch");
    }

    void Player_PlayerInfectionComplete(Player p)
    {

            globalAudioManager.Stop("Infection1");

    }







    // Update is called once per frame
    void Update()
    {
        
    }



}
