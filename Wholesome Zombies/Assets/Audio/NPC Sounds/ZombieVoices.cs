﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieVoices : MonoBehaviour
{


    public LocalAudioManager localAudio;
    public Person zombie;

    // Start is called before the first frame update
    void Start()
    {
        zombie.NPCInfected += Zombie_NPCInfected;
        zombie.NPCCured += Zombie_NPCCured;

    }

    void Zombie_NPCInfected()
    {
        int n = Random.Range(1, 3);

        localAudio.Play("H2Z" + n);
    }

    void Zombie_NPCCured()
    {

            int n = Random.Range(1, 8);
            localAudio.Play("Z2H" + n);



    }


}
