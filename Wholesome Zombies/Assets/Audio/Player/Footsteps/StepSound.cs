﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    public LocalAudioManager localAudio;
    public float checktime;
    private float timer;

    public float stepDist;
    private Vector3 currentPos;
    // Update is called once per frame
    void Update()
    {

        
        timer += Time.deltaTime;
        if (timer > checktime)
        {
            if ((currentPos - transform.position).magnitude > stepDist)
            {
                int n = Random.Range(1, 5);
                localAudio.Play("Concrete" + n);
            }
            currentPos = transform.position;
            timer = 0;
        }
        

    }
}
