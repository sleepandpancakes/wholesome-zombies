﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour
{
    public LocalAudioManager localAudio;
    public float volumeMultiplier = 1;

    private void OnDestroy()
    {
        int n = Random.Range(1, 6);
        localAudio.Play("Explosion" + n, transform.position, volumeMultiplier);
    }
}
