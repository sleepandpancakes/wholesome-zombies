﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ValueEventArgs<T> : EventArgs
{
    public T Value { get; private set; }

    public ValueEventArgs(T value)
    {
        Value = value;
    }
}
