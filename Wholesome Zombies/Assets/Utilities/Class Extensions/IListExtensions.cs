﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class IListExtensions {

	public static T RandomElement<T>(this IList<T> collection)
	{
		return collection[Random.Range(0, collection.Count)];
	}

	public static void Shuffle<T>(this IList<T> collection, System.Random random)
	{
		int n = collection.Count;
		for (int i = 0; i < n; i++)
		{
			// NextDouble returns a random number between 0 and 1.
			// ... It is equivalent to Math.random() in Java.
			int r = i + (int)(random.NextDouble() * (n - i));
			T t = collection[r];
			collection[r] = collection[i];
			collection[i] = t;
		}
	}
		
	public static void Shuffle<T>(this IList<T> collection)
	{
		int n = collection.Count;
		for (int i = 0; i < n; i++)
		{
			// NextDouble returns a random number between 0 and 1.
			// ... It is equivalent to Math.random() in Java.
//			int r = i + (int)(random.NextDouble() * (n - i));
			int r = Random.Range(i, n);
			T t = collection[r];
			collection[r] = collection[i];
			collection[i] = t;
		}
	}

	public static void QuantifiedShuffle<T>(this IList<T> collection, float shufflingPower)
	{
		int n = collection.Count;
		int numSwaps = (int)((float)n * shufflingPower);

		for (int i = 0; i < numSwaps; i++)
		{
			int r = Random.Range(i, n);
			T t = collection[r];
			collection[r] = collection[i];
			collection[i] = t;
		}
	}

	// 

	public static int GetMode(this IList<int> array)
	{
		return array
			.GroupBy(x => x)
			.OrderByDescending(g => g.Count())
			.First() // throws InvalidOperationException if myArray is empty
			.Key;
	}
		
}
