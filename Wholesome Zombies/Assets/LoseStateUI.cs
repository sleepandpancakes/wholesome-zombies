﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseStateUI : MonoBehaviour
{
    public Text loseText;
    public Text respawningText;
    //public GameObject infectionAnimation;
    public Animator infectionAnimator;

    private bool isLoser = false;

    // Start is called before the first frame update
    void Start()
    {
        Player.PlayerInfectionStarted += Player_PlayerInfectionStarted;
        Player.PlayerInfectionComplete += Player_PlayerInfectionComplete;
        PlayerTracker.GameLost += PlayerTracker_GameLost;

    }

    void Player_PlayerInfectionStarted(Player p)
    {
        infectionAnimator.SetBool("Infecting", true);

    }

    void Player_PlayerInfectionComplete(Player p)
    {

        if (isLoser)
        {

        }
        else
        {

            infectionAnimator.SetBool("Infecting", false);
        }
    }

    void PlayerTracker_GameLost()
    {
        isLoser = true;
    }

}


